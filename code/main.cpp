#include <stdio.h>
#include <stdlib.h>

#define INI_IMPLEMENTATION
#include "ini.h"

int
main(void)
{
	FILE *File = fopen("../code/test.ini", "rb");
	fseek(File, 0, SEEK_END);
	int Size = ftell(File);
	fseek(File, 0, SEEK_SET);

	unsigned char *FileContents = (unsigned char *)malloc(sizeof(unsigned char)*Size + 1);
	fread(FileContents, 1, Size, File);
	FileContents[Size] = 0;

	ini_t Ini = ini_load((char *)FileContents);
	fclose(File);
	free(FileContents);

	printf("Some property: %s\n", ini_get_value_by_name_and_section(&Ini, "name_conflict", "third_section"));
	ini_set_value_by_name(&Ini, "name_conflict", "short");
	printf("Some property: %s\n", ini_get_value_by_name(&Ini, "name_conflict"));
	ini_set_value_by_name(&Ini, "name_conflict", "really really super duper very long way longer that the original was");
	printf("Some property: %s\n", ini_get_value_by_name(&Ini, "name_conflict"));
	ini_add_item(&Ini, "super_cool_thingy", "secret_section", "my super cool value");
	printf("Some property: %s\n", ini_get_value_by_name_and_section(&Ini, "super_cool_thingy", "secret_section"));
	ini_set_value_by_name_and_section(&Ini, "super_cool_thingy", "secret_section", "a super cool value");
	printf("Some property: %s\n", ini_get_value_by_name_and_section(&Ini, "super_cool_thingy", "secret_section"));
	ini_release(&Ini);

	ini_t NewIni = ini_create();
	ini_add_item(&NewIni, "zoom_amount", 0, "10");
	ini_add_item(&NewIni, "zoom_size", 0, "10");
	ini_add_item(&NewIni, "width", "screen", "1920");
	ini_add_item(&NewIni, "height", "screen", "1080");

	printf("%s\n", ini_get_value_by_name(&NewIni, "width"));
	int ScreenWidth = atoi(ini_get_value_by_name(&NewIni, "width"));
	int ScreenHeight = atoi(ini_get_value_by_name(&NewIni, "height"));
	printf("%dx%d\n", ScreenWidth, ScreenHeight);
	ini_set_value_by_name(&NewIni, "width", "2550");
	ini_set_value_by_name_and_section(&NewIni, "height", "screen", "1440");
	ScreenWidth = atoi(ini_get_value_by_name(&NewIni, "width"));
	ScreenHeight = atoi(ini_get_value_by_name(&NewIni, "height"));
	printf("%dx%d\n", ScreenWidth, ScreenHeight);
	int WriteSize = 0;
	char *NewIniData = ini_write(&NewIni, &WriteSize);
	ini_release(&NewIni);

	File = fopen("../code/write.dat", "wb");
	fwrite(NewIniData, 1, WriteSize, File);
	fclose(File);
	free(NewIniData);

	return 0;
}
