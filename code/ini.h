/**
 *
 * ini.h - Simple INI file reader/writer
 *
 * Do this:
 *	#define INI_IMPLEMENTATION
 * Before including this file in *one* C/C++ file to create the implementation.
 *
 */

#ifndef INI_H


struct ini_section;
struct ini_item;
struct ini_t;

struct ini_t ini_load(char *Data);
struct ini_t ini_create();
void ini_release(struct ini_t *Ini);
char * ini_write(struct ini_t *IniCtx, int *SizeOut);

char * ini_get_value_by_name_and_section(struct ini_t *IniCtx, char *Name, char *Section);
char * ini_get_value_by_name(struct ini_t *IniCtx, char *Name);

int ini_set_value_by_name_and_section(struct ini_t *IniCtx, char *Name, char *Section, char *Value);
int ini_set_value_by_name(struct ini_t *IniCtx, char *Name, char *Value);

int ini_add_item(struct ini_t *IniCtx, char *Name, char *Section, char *Value);

/**
 *
 * Example
 * =======
 *
 * #include <stdio.h>
 * #include <stdlib.h>
 *
 * #define INI_IMPLEMENTATION
 * #include "ini.h"
 *
 * int main(void)
 * {
 *		FILE *File = fopen("settings.ini", "rb");
 *		fseek(File, 0, SEEK_END);
 *		int Size = ftell(File);
 *		fseek(File, 0, SEEK_SET);
 *		char *Data = (char *)malloc(Size);
 *
 *		ini_t Settings = ini_load(Data);
 *		free(Data);
 *		fclose(File);
 *
 *		int Width = atoi(ini_get_value_by_name(&Settings, "Width"));
 *		ini_set_value_by_name(&Settings, "Height", "100");
 *		ini_add_item(&Settings, "NewKey", "NewSection", "New Value");
 *
 *		int Length = 0;
 *		char *NewFile = ini_write(&Settings, &Length);
 *		File = fopen("output.ini", "wb");
 *		fwrite(NewFile, 1, Length, File);
 *		fclose(File);
 *		free(NewFile);
 *
 *		ini_release(&Settings);
 *		return 0;
 * }
 *
 * Customization
 * =============
 *	#define INI_MAX_ITEMS
 * 
 * Defines the maximum number of key/value pairs ini_t objects can store.
 * Defaults to 200.
 *
 *	#define INI_MAX_SECTIONS
 * Defines the maximum number of sections ini_t objects can contain. Defaults to
 * 50.
 *
 * API Documentation
 * =================
 *
 * ini_load
 * --------
 *	struct ini_t ini_load(char *Data)
 *
 * Parses a zero terminated string `Data` containing ini-file structured
 * contents and returns a new ini_t object containing the parsed key/value pairs
 * along with the sections. This object should be manipulated using the other
 * API functions. When no longer needed memory can be released by calling
 * `ini_release`.
 *
 * ini_create
 * ----------
 *	struct ini_t ini_create()
 *
 * Creates a new empty ini_t object which can be filled with new key/value pairs
 * and sections using the other API functions. The contents can be saved using
 * `ini_write`. When no longer needed memory can be released by calling
 * `ini_release`.
 *
 * ini_release
 * -----------
 *	void ini_release(struct ini_t *Ini)
 *
 * Frees the memory allocated by the ini_t object `Ini`.
 *
 * ini_write
 * ---------
 *	char * ini_write(struct ini_t *IniCtx, int *SizeOut)
 *
 * Writes all key/value pairs grouped by sections contained within `IniCtx`.
 * `SizeOut` is an optional parameter which is used to return the length of the
 * string returned by the function.
 *
 * *Important* the user of this function is responsible for the memory allocated
 * and returned by this function.
 *
 * ini_get_value_by_name_and_section
 * ---------------------------------
 *	char * ini_get_value_by_name_and_section(struct ini_t *IniCtx, char *Name, char *Section)
 *
 * Finds a key using its `Name` and `Section` combination in the ini_t object
 * `IniCtx`. Returns NULL if the name/section combination cannot be found. If
 * the name and section combination can be resolved then a pointer to the value
 * string is returned.
 *
 * ini_get_value_by_name
 * ---------------------
 *	char * ini_get_value_by_name(struct ini_t *IniCtx, char *Name)
 *
 * Finds a key by `Name` in the ini_t object `IniCtx`. If the key is found then
 * a pointer to the value string is returned, otherwise NULL is returned. For
 * multiple keys sharing the same name this function will return the first in
 * the file, in this case you should use ini_get_value_by_name_and_section.
 *
 * ini_set_value_by_name_and_section
 * ---------------------------------
 *	int ini_set_value_by_name_and_section(struct ini_t *IniCtx, char *Name, char *Section, char *Value)
 *
 * Sets the `Value` of a key by `Name` and `Section` of the ini_t object
 * `IniCtx`. If the value is successfully set this function returns 1, otherwise
 * 0 is returned.
 *
 * ini_set_value_by_name
 * ---------------------
 *	int ini_set_value_by_name(struct ini_t *IniCtx, char *Name, char *Value)
 *
 * Sets the `Value` of a `Key` in an ini_t object `IniCtx`. Returns 1 if the key
 * is successfully set, otherwise 0. For cases where multiple keys share the
 * same name this function will set the first in the file, in this case you
 * should use ini_set_value_by_name_and_section.
 *
 * ini_add_item
 * ------------
 *	int ini_add_item(struct ini_t *IniCtx, char *Name, char *Section, char *Value)
 *
 * Adds a new key to the ini_t object `IniCtx` with the `Name`, `Section` and
 * `Value`. `Section` can be NULL, signaling that the key belongs to the global
 * section. Returns 1 if the key is added, 0 otherwise.
 *
 */

#define INI_H
#endif

#ifdef INI_IMPLEMENTATION

#ifndef INI_MAX_ITEMS
#define INI_MAX_ITEMS 200
#endif

#ifndef INI_MAX_SECTIONS
#define INI_MAX_SECTIONS 50
#endif

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct ini_section
{
	char *Name;
	int NameLength;
};

struct ini_item
{
	char *Key;
	int KeyLength;

	char *Value;
	int ValueLength;

	struct ini_section *Section;
};

struct ini_t
{
	ini_item *Settings;
	int SettingsCount;
	int MaxSettingsCount;

	ini_section *Sections;
	int SectionsCount;
	int MaxSectionsCount;

	ini_section *LastSection;

	unsigned char *FileData;
};

struct ini_t
ini_create()
{
	struct ini_t Result = {0};

	Result.SettingsCount = 0;
	Result.MaxSettingsCount = INI_MAX_ITEMS;
	Result.Settings = (ini_item *)malloc(sizeof(ini_item) * INI_MAX_ITEMS);

	Result.SectionsCount = 0;
	Result.MaxSectionsCount = INI_MAX_SECTIONS;
	Result.Sections = (ini_section *)malloc(sizeof(ini_section) * INI_MAX_SECTIONS);

	Result.LastSection = NULL;

	return(Result);
}

void ini_parse(struct ini_t *Ini);
struct ini_t
ini_load(char *Data)
{
	struct ini_t Result = {0};
	int Length = strlen(Data);

	Result.FileData = (unsigned char *)malloc(sizeof(unsigned char) * Length + 1);
	Result.FileData[Length] = 0;
	memcpy(Result.FileData, Data, Length);

	Result.SettingsCount = 0;
	Result.MaxSettingsCount = INI_MAX_ITEMS;
	Result.Settings = (ini_item *)malloc(sizeof(ini_item) * INI_MAX_ITEMS);

	Result.SectionsCount = 0;
	Result.MaxSectionsCount = INI_MAX_SECTIONS;
	Result.Sections = (ini_section *)malloc(sizeof(ini_section) * INI_MAX_SECTIONS);

	ini_parse(&Result);

	return(Result);
}

void
ini_release(struct ini_t *Ini)
{
	if(Ini)
	{
		if(Ini->FileData)
		{
			free(Ini->FileData);
		}

		for(int SettingIndex = 0; SettingIndex < Ini->SettingsCount; ++SettingIndex)
		{
			struct ini_item *Item = Ini->Settings + SettingIndex;
			free(Item->Key);
			free(Item->Value);
		}
		free(Ini->Settings);

		for(int SectionIndex = 0; SectionIndex < Ini->SectionsCount; ++SectionIndex)
		{
			struct ini_section *Section = Ini->Sections + SectionIndex;
			free(Section->Name);
		}
		free(Ini->Sections);
	}
}

static ini_section *
ini_get_section_by_name(struct ini_t *IniCtx, char *Name)
{
	struct ini_section *Result = 0;

	for(int SectionIndex = 0; SectionIndex < IniCtx->SectionsCount; ++SectionIndex)
	{
		struct ini_section *TestSection = IniCtx->Sections + SectionIndex;
		if(strcmp(Name, TestSection->Name) == 0)
		{
			Result = TestSection;
			break;
		}
	}

	return(Result);
}

static struct ini_item *
ini_get_ini_item(struct ini_t *IniCtx, char *Name, char *Section)
{
	struct ini_item *Result = 0;

	struct ini_section *IniSection = NULL;
	if(Section != NULL)
	{
		IniSection = ini_get_section_by_name(IniCtx, Section);
	}

	for(int SettingIndex = 0; SettingIndex < IniCtx->SettingsCount; ++SettingIndex)
	{
		struct ini_item *Setting = IniCtx->Settings + SettingIndex;
		if(Section != NULL)
		{
			if(Setting->Section == IniSection)
			{
				if(strcmp(Name, Setting->Key) == 0)
				{
					Result = Setting;
					break;
				}
			}
		}
		else
		{
			if(strcmp(Name, Setting->Key) == 0)
			{
				Result = Setting;
				break;
			}
		}
	}

	return(Result);
}

char *
ini_get_value_by_name_and_section(struct ini_t *IniCtx, char *Name, char *Section)
{
	char *Result = 0;

	struct ini_section *IniSection = NULL;
	if(Section != NULL)
	{
		IniSection = ini_get_section_by_name(IniCtx, Section);
	}

	for(int SettingIndex = 0; SettingIndex < IniCtx->SettingsCount; ++SettingIndex)
	{
		struct ini_item *Setting = IniCtx->Settings + SettingIndex;
		if(Section != NULL)
		{
			if(Setting->Section == IniSection)
			{
				if(strcmp(Name, Setting->Key) == 0)
				{
					Result = Setting->Value;
					break;
				}
			}
		}
		else
		{
			if(strcmp(Name, Setting->Key) == 0)
			{
				Result = Setting->Value;
				break;
			}
		}
	}

	return(Result);
}

char *
ini_get_value_by_name(struct ini_t *IniCtx, char *Name)
{
	char *Result = 0;
	Result = ini_get_value_by_name_and_section(IniCtx, Name, 0);
	return(Result);
}

int
ini_set_value_by_name_and_section(struct ini_t *IniCtx, char *Name, char *Section, char *Value)
{
	int Result = 0;

	struct ini_item *Item = ini_get_ini_item(IniCtx, Name, Section);
	if(Item != NULL)
	{
		int NewValueLength = strlen(Value);
		if(NewValueLength > Item->ValueLength)
		{
			free(Item->Value);
			Item->Value = (char *)malloc(sizeof(char) * (NewValueLength+1));
			memcpy(Item->Value, Value, NewValueLength);
			Item->Value[NewValueLength] = 0;
			Item->ValueLength = NewValueLength;
		}
		else
		{
			memcpy(Item->Value, Value, NewValueLength);
			Item->Value[NewValueLength] = 0;
			Item->ValueLength = NewValueLength;
		}
	}

	return(Result);
}

int
ini_set_value_by_name(struct ini_t *IniCtx, char *Name, char *Value)
{
	int Result = ini_set_value_by_name_and_section(IniCtx, Name, 0, Value);
	return(Result);
}

int
ini_add_item(struct ini_t *IniCtx, char *Name, char *Section, char *Value)
{
	int Result = 0;

	if(IniCtx->SettingsCount < IniCtx->MaxSettingsCount)
	{
		struct ini_item *NewSetting = IniCtx->Settings + IniCtx->SettingsCount++;
		NewSetting->KeyLength = strlen(Name);
		NewSetting->Key = (char *)malloc(sizeof(char) * (NewSetting->KeyLength+1));
		memcpy(NewSetting->Key, Name, NewSetting->KeyLength);
		NewSetting->Key[NewSetting->KeyLength] = 0;
		NewSetting->ValueLength = strlen(Value);
		NewSetting->Value = (char *)malloc(sizeof(char) * (NewSetting->ValueLength+1));
		memcpy(NewSetting->Value, Value, NewSetting->ValueLength);
		NewSetting->Value[NewSetting->ValueLength] = 0;

		if(Section != NULL)
		{
			struct ini_section *SettingSection = ini_get_section_by_name(IniCtx, Section);
			if(SettingSection == NULL)
			{
				if(IniCtx->SectionsCount < IniCtx->MaxSectionsCount)
				{
					int SectionNameLength = strlen(Section);
					SettingSection = IniCtx->Sections + IniCtx->SectionsCount++;
					SettingSection->Name = (char *)malloc(sizeof(char) * (SectionNameLength+1));
					memcpy(SettingSection->Name, Section, SectionNameLength);
					SettingSection->Name[SectionNameLength] = 0;
					SettingSection->NameLength = SectionNameLength;
				}
			}
			NewSetting->Section = SettingSection;
		}
		else
		{
			NewSetting->Section = NULL;
		}

		Result = 1;
	}

	return(Result);
}

// NOTE(rick): Returns a pointer to a CString, user is responsible for releasing
// memory when they're done with it.
char *
ini_write(struct ini_t *IniCtx, int *SizeOut)
{
	int BufferSize = 0;
	for(int SectionIndex = 0; SectionIndex < IniCtx->SectionsCount; ++SectionIndex)
	{
		ini_section *Section = IniCtx->Sections + SectionIndex;
		BufferSize += (Section->NameLength + 5);
	}

	for(int SettingIndex = 0; SettingIndex < IniCtx->SettingsCount; ++SettingIndex)
	{
		ini_item *Setting = IniCtx->Settings + SettingIndex;
		BufferSize += (Setting->KeyLength + 5);
		BufferSize += (Setting->ValueLength + 5);
	}

	char *ResultBuffer = (char *)malloc(sizeof(char *) * BufferSize);
	memset(ResultBuffer, 0x00, BufferSize);
	char *WritePtr = ResultBuffer;

	for(int SectionIndex = -1; SectionIndex < IniCtx->SectionsCount; ++SectionIndex)
	{
		ini_section *Section = NULL;
		if(SectionIndex != -1)
		{
			Section = IniCtx->Sections + SectionIndex;
		}

		if(Section)
		{
			WritePtr += snprintf(WritePtr, BufferSize, "[%.*s]\n", Section->NameLength, Section->Name);
		}

		for(int SettingIndex = 0; SettingIndex < IniCtx->SettingsCount; ++SettingIndex)
		{
			ini_item *Setting = IniCtx->Settings + SettingIndex;
			if(Setting->Section == Section)
			{
				WritePtr += snprintf(WritePtr, BufferSize, "%.*s=%.*s\n", Setting->KeyLength,
									 Setting->Key, Setting->ValueLength, Setting->Value);
			}
		}

	}

	if(SizeOut)
	{
		*SizeOut = WritePtr - ResultBuffer;
	}

	return ResultBuffer;
}

static void
ConsumeWhitespace(char **Buffer)
{
	while(isspace(**Buffer))
	{
		++(*Buffer);
	}
}

static int
ini_read_line(char *Buffer, int MaxLength, char *Source)
{
	int Length = 0;
	while((*Source != 0) &&
		  (Length < MaxLength))
	{
		if((*Source == '\n') ||
		   (*Source == '\r'))
		{
			Buffer[Length++] = 0;
			break;
		}

		Buffer[Length++] = *Source++;
	}

	if(Length)
	{
		Buffer[Length++] = 0;
	}

	return(Length);
}

static int
ini_key_length(char *Buffer, int MaxLength, char **KeyStart)
{
	int Length = 0;

	ConsumeWhitespace(&Buffer);
	if(KeyStart != NULL)
	{
		*KeyStart = Buffer;
	}

	while((*Buffer != '=') &&
		  (Length < MaxLength))
	{
		if(*Buffer == 0)
		{
			return -1;
		}

		++Length, ++Buffer;
	}

	return(Length);
}

static int
ini_value_length(char *Buffer, int MaxLength, char **ValueStart)
{
	int Length = 0;

	int ValueOffset = ini_key_length(Buffer, MaxLength, 0) + 1;
	Buffer += ValueOffset;
	ConsumeWhitespace(&Buffer);
	if(ValueStart != NULL)
	{
		*ValueStart = Buffer;
	}

	while((*Buffer != 0) &&
		  (*Buffer != '\r') &&
		  (*Buffer != '\n'))
	{
		++Buffer, ++Length;
	}

	return(Length);
}

static int
ini_section_length(char *Buffer, int MaxLength, char **SectionStart)
{
	int Length = 0;
	int Counting = 0;

	while(*Buffer != 0)
	{
		if((*Buffer == '[') && 
		   (Counting == 0))
		{
			Counting = 1;
			++Buffer;

			if(SectionStart != NULL)
			{
				*SectionStart = Buffer;
			}
		}

		if((*Buffer == ']') &&
		   (Counting == 1))
		{
			Counting = 0;
			break;
		}

		if(Counting)
		{
			++Length;
		}
		++Buffer;
	}

	return(Length);
}

#define INI_INTERNAL_MAX_LINE_LENGTH 512
void
ini_parse(struct ini_t *Ini)
{
	char ReadBuffer[INI_INTERNAL_MAX_LINE_LENGTH] = {0};
	char *Reader = (char *)Ini->FileData;

	int ReadBytes = 0;
	while((ReadBytes = ini_read_line(ReadBuffer, INI_INTERNAL_MAX_LINE_LENGTH, Reader)) > 0)
	{
		Reader += ReadBytes;
		int StringLength = strlen(ReadBuffer);
		char *BufferReader = ReadBuffer;
		ConsumeWhitespace(&BufferReader);
		if(StringLength > 0)
		{
			if(BufferReader[0] == ';')
			{
				// NOTE(rick): Skipping comment lines
				continue;
			}
			else if(BufferReader[0] == '[')
			{
				if(Ini->SectionsCount < Ini->MaxSectionsCount)
				{
					char *SectionStart = 0;
					int SectionLength = ini_section_length(BufferReader, StringLength, &SectionStart);

					struct ini_section *NewSection = Ini->Sections + Ini->SectionsCount++;
					NewSection->Name = (char *)malloc(sizeof(char) * (SectionLength+1));
					memcpy(NewSection->Name, SectionStart, SectionLength);
					NewSection->Name[SectionLength] = 0;
					NewSection->NameLength = SectionLength;
					Ini->LastSection = NewSection;
				}
			}
			else
			{
				if(Ini->SettingsCount < Ini->MaxSettingsCount)
				{
					char *KeyStart = 0;
					int KeyLength = ini_key_length(BufferReader, StringLength, &KeyStart);

					char *ValueStart = 0;
					int ValueLength = ini_value_length(BufferReader, StringLength, &ValueStart);

					struct ini_item *NewSetting = Ini->Settings + Ini->SettingsCount++;
					NewSetting->KeyLength = KeyLength;
					NewSetting->Key = (char *)malloc(sizeof(char) * (KeyLength+1));
					memcpy(NewSetting->Key, KeyStart, KeyLength);
					NewSetting->Key[KeyLength] = 0;
					NewSetting->ValueLength = ValueLength;
					NewSetting->Value = (char *)malloc(sizeof(char) * (ValueLength+1));
					memcpy(NewSetting->Value, ValueStart, ValueLength);
					NewSetting->Value[ValueLength] = 0;
					NewSetting->Section = Ini->LastSection;
				}
			}
		}
	}
	free(Ini->FileData);
	Ini->FileData = NULL;
}
#undef INI_INTERNAL_MAX_LINE_LENGTH

#endif
