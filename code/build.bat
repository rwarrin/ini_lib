@echo off

if not exist ..\build mkdir ..\build
pushd ..\build
cl /nologo -fp:fast /MTd /Z7 ..\code\main.cpp /link /incremental:no
popd
